import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { Observable, observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  readonly APIUrl = "http://localhost:50893/api";
  readonly PhotoUrl = "http://localhost:50893/Photos";

  constructor(private http:HttpClient) { }

  getDepList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl + '/department');
  }

  addDepartment(val:any)
  {
    return this.http.post(this.APIUrl + '/Department', val);
  }

  updateDepartment(val:any)
  {
    return this.http.put(this.APIUrl+'/Department', val);
  }

  deleteDepartment(val: any)
  {
    return this.http.delete(this.APIUrl+'/Department/'+ val);
  }



  getEmployeeList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl+'/Employee');
  }

  addEmployee(val:any)
  {
    return this.http.post(this.APIUrl+'/Employee', val);
  }

  updateEmployee(val:any)
  {
    return this.http.put(this.APIUrl+'/Employee', val);
  }

  deleteEmployee(val:any)
  {
    return this.http.delete(this.APIUrl+'/Employee/' + val);
  }


  UploadPhoto(val:any){
    return this.http.post(this.APIUrl + '/Employee/SaveFile', val);
  }

  getAllDepartmentNames():Observable<any>{
    return this.http.get<any[]>(this.APIUrl + '/Employee/GetAllDepartmentNames');
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Http;
using EmployeeManagmentApp.Models;


namespace EmployeeManagmentApp.Controllers
{
    public class DepartmentController : ApiController
    {
        // GET: Department
        public HttpResponseMessage GetDepartment()
        {
            string query = @"select DepartmentId,DepartmentName from dbo.Department";
            DataTable dataTable = new DataTable();

            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmployeeMGMT_App"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(dataTable);
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataTable);
        }

        public string Post(Department dep)
        {
            try
            {
                string query = @"insert into dbo.Department values ('" + dep.DepartmentName + @"')";
                DataTable dataTable = new DataTable();

                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmployeeMGMT_App"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(dataTable);
                }
                return "Department added succesfully!";
            }
            catch (Exception)
            {

                return "Failed to add Department";
            }
        }

        public string Put(Department dep)
        {
            try
            {
                string query = @"update dbo.Department set DepartmentName='" + dep.DepartmentName + @"'where DepartmentId=" + dep.DepartmentId + @"";
                DataTable dataTable = new DataTable();

                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmployeeMGMT_App"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(dataTable);
                }
                return "Department update succesfully!";
            }
            catch (Exception)
            {

                return "Failed to update Department";
            }
        }

        public string Delete(int id)
        {
            try
            {
                string query = @"delete from dbo.Department where DepartmentId=" + id + @"";
                DataTable dataTable = new DataTable();

                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmployeeMGMT_App"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(dataTable);
                }
                return "Department delete succesfully!";
            }
            catch (Exception)
            {

                return "Failed to delete Department";
            }
        }
    }
}
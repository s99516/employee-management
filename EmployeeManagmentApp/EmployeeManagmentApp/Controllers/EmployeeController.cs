﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Web.Mvc;
using System.Net.Http;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Http;
using EmployeeManagmentApp.Models;

namespace EmployeeManagmentApp.Controllers
{
    public class EmployeeController : ApiController
    {
        // GET: Employee
        public HttpResponseMessage GetEmployee()
        {
            string query = @"select * from dbo.Employee";
            DataTable dataTable = new DataTable();

            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmployeeMGMT_App"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(dataTable);
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataTable);
        }

        // POST: Employee
        public string Post(Employee emp)
        {
            try
            {
                string query = @"
                    insert into dbo.Employee values
                    (
                    '" + emp.FirstName + @"'
                    ,'" + emp.LastName + @"'
                    ,'" + emp.Department + @"'
                    ,'" + emp.DateOfJoining + @"'
                    ,'" + emp.PhotoFileName + @"'
                    )
                    ";
                DataTable dataTable = new DataTable();

                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmployeeMGMT_App"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(dataTable);
                }
                return "Employee added succesfully!";
            }
            catch (Exception)
            {

                return "Failed to add employee";
            }
        }

        // PUT: Employee
        public string Put(Employee emp)
        {
            try
            {
                string query = @"
                    update dbo.Employee set 
                    FirstName='" + emp.FirstName + @"'
                    ,LastName='" + emp.LastName + @"'
                    ,Department='" + emp.Department + @"'
                    ,DateOfJoining='" + emp.DateOfJoining + @"'
                    ,PhotoFileName='" + emp.PhotoFileName + @"'
                    where EmployeeId=" + emp.EmployeeId + @"
                    ";
                DataTable dataTable = new DataTable();

                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmployeeMGMT_App"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(dataTable);
                }
                return "Employee updated succesfully!";
            }
            catch (Exception)
            {

                return "Failed to update employee";
            }
        }
        // DELETE: Employee
        public string Delete(int id)
        {
            try
            {
                string query = @"delete from dbo.Employee where EmployeeId=" + id + @"";
                DataTable dataTable = new DataTable();

                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmployeeMGMT_App"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(dataTable);
                }
                return "Employee delete succesfully!";
            }
            catch (Exception)
            {

                return "Failed to delete Employee";
            }
        }

        [Route("api/Employee/GetAllDepartmentNames")]
        [HttpGet]
        public HttpResponseMessage GetAllDepartmentNames()
        {
            string query = @"select DepartmentName from dbo.Department";
            DataTable dataTable = new DataTable();

            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmployeeMGMT_App"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(dataTable);
            }
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataTable);
        }
        [Route("api/Empoyee/SaveFile")]
        public string SaveFile()
        {
            try
            {
                var httpRequest = HttpContext.Current.Request;
                var postedFile = httpRequest.Files[0];
                string fileName = postedFile.FileName;
                var physicalPath = HttpContext.Current.Server.MapPath("~/Photos/" + fileName);

                postedFile.SaveAs(physicalPath);
                return fileName;
            }
            catch (Exception)
            {

                return "anonymous.png";
            }
        }
    }
}